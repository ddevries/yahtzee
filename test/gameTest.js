/* global Player */

"use strict";

(function () {
    runTest("gameTest, should win game with thrown value 6", result, true);

    function result() {
        var mockDie = function () {
            this.throwDie = function () {
                return 6;
            };
        };

        var game = new Game(Player, mockDie);
        game.throwDie();
        return game.hasWon();
    }
})();

(function () {
    for (var i = 1; i <= 5; i++) {
        var result = function () {
            var mockDie = function () {
                this.throwDie = function () {
                    return i;
                };
            };

            var game = new Game(Player, mockDie);
            game.throwDie();
            return game.hasWon();
        };

        runTest("gameTest, should lose game with thrown value " + i, result, false);
    }
})();

(function () {
    runTest("gameTest, should return result and reset score", result, false);

    function result() {
        var mockDie = function () {
            this.throwDie = function () {
                return 6;
            };
        };

        var game = new Game(Player, mockDie);
        game.throwDie();
        game.hasWon();
        return game.hasWon();
    }
})();