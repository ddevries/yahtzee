"use strict";

(function () {
    runTest("yahtzeeViewControllerTest, should throw die and print result when won", result, 'Please throw the die.<br>You have thrown: 6<br>You have won!');

    function result() {
        var mockGame = {
            throwDie: function () {
                return 6;
            },
            hasWon: function () {
                return true;
            }
        };

        var mockView = document.createElement('div');
        mockView.innerHTML = 'Please throw the die.';

        var controller = new YahtzeeViewController(mockGame, mockView);
        controller.roll();

        return mockView.innerHTML;
    }
})();

(function () {
    runTest("yahtzeeViewControllerTest, should throw die and print result when lost", result, 'Please throw the die.<br>You have thrown: 5<br>You lost!');

    function result() {
        var mockGame = {
            throwDie: function () {
                return 5;
            },
            hasWon: function () {
                return false;
            }
        };

        var mockView = document.createElement('div');
        mockView.innerHTML = 'Please throw the die.';

        var controller = new YahtzeeViewController(mockGame, mockView);
        controller.roll();

        return mockView.innerHTML;
    }
})();