"use strict";

(function () {
    runTest("dieTest, should return number", result, 'number');

    function result() {
        var die = new Die();
        return typeof die.throwDie();
    }
})();