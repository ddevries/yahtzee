"use strict";

(function () {  
    runTest("playerTest, score should be 0 on init", result, 0);

    function result() {
        var player = new Player();
        return player.getScore();
    }
})();

(function () {
    var expectedResult = 4;
    
    runTest("playerTest, should set score and get previous score", result, expectedResult);

    function result() {
        var player = new Player();
        player.setScore(expectedResult);
        return player.getScore();
    }
})();


(function () {
    var expectedResult = 4;
    
    runTest("playerTest, should not set score when value to set is not a number", result, expectedResult);

    function result() {
        var player = new Player();
        player.setScore(expectedResult);
        player.setScore('not a number');
        return player.getScore();
    }
})();

(function () {    
    runTest("playerTest, should reset score", result, 0);

    function result() {
        var player = new Player();
        player.setScore(4);
        player.resetScore();
        return player.getScore();
    }
})();