"use strict";

function runTest(name, result, expectedResult) {
        console.log(name + ": " + test(result, expectedResult));
        
        function test(result, expectedResult) {
            var testResult;
            try {
                result = (typeof result === 'function' ? result() : result);
                testResult = (result === expectedResult) ? 'passed' : 'failed, ' + conditionNotMet(result, expectedResult);
            } catch (err) {
                testResult = 'failed, ' + err;
            }
            
            return testResult;
        }

        function conditionNotMet(result, expectedResult) {
            var resultString = (typeof result === 'string') ? "'" + result + "'" : result;
            var expectedResultString = (typeof expectedResult === 'string') ? "'" + expectedResult + "'" : expectedResult;

            return 'condition not met. ' + resultString + ' should equal ' + expectedResultString;
        }
}