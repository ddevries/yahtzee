"use strict";

function Die() {
    return {
        throwDie: function () {
            return _randomNumber();
        }
    };

    function _randomNumber() {
        return Math.floor(Math.random() * 6) + 1;
    }
}