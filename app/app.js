/* global YahtzeeViewController, Player, Game, Die */

"use strict";

window.addEventListener("load", function () {
    window.APP = (function () {
        var _game = new Game(Player, Die);
        var _view = document.getElementById("view");
        var _controller = new YahtzeeViewController(_game, _view);

        return {
            controller: _controller
        };
    })();
});