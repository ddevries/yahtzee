"use strict";

function YahtzeeViewController(game, view) {   
    return {
        roll: function () {
            _roll();
        }
    };

    function _roll() {
        view.innerHTML += "<br>You have thrown: " + game.throwDie();
        view.innerHTML += game.hasWon() ? '<br>You have won!': '<br>You lost!';
    }
}