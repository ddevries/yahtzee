"use strict";

function Game(Player, Die) {
    var _player = new Player();
    var _die = new Die();

    return {
        throwDie: function () {
            return _throwDie(_player);
        },
        hasWon: function () {
            return _hasWon(_player);
        }
    };

    function _hasWon(player) {
        var result = player.getScore();
        player.resetScore();
        return result === 6;
    }

    function _throwDie(player) {
        return player.setScore(_die.throwDie());
    }
}