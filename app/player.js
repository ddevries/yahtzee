"use strict";

function Player() {
    var _initValue = 0;
    var _score = _initValue;

    return {
        setScore: function (scoreToSet) {
           return _setScore(scoreToSet);
        },
        getScore: function () {
            return _score;
        },
        resetScore: function () {
            return _setScore(_initValue);
        }
    };
    
    function _setScore(scoreToSet) {
        return _score = (typeof scoreToSet === 'number' ? scoreToSet : _score);
    }
}